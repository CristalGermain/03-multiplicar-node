const config = {
    base: {
        demand: true, // Define si es obligatorio o no
        alias: 'b'
    },
    limite: {
        alias: 'l',
        default: 10 // Valor predeterminado
    }
};



// '.command' permite especificar qué comandos pueden usarse al ejecutar el archivo

const argv = require('yargs')
    .command('listar', 'Imprime en consola una tabla de multiplicar', config)
    .command('crear', 'Crea un archivo txt donde muestra una tabla de multiplicar', config)
    .help() // Yargs permite una serie de información como ayuda
    .argv;


module.exports = {
    argv
}