const { crearArchivo } = require('./multiplicar/multiplicar');


//process es una variable que se genera al correr Node y almacena muchos datos

//argv son los argumentos que se le mandan al ejecutar node. Por ejemplo, en el comando 'npm install express', 'install' y 'express' son args.

//En la siguiente línea obtengo la posición 3 de los argumentos. La posición 0 es el path de Node; la 1, el path del archivo que se está "corriendo"
let argv = process.argv[2];
let base = argv.split('=')[1]; //Split separa una cadena en un array, dependiento del parámetro a mandar


crearArchivo(base)
    .then(archivo => console.log(`Archivo creado: ${archivo}`))
    .catch(err => console.log(err));