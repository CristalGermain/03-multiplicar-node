

En esta sección aprendí que Node cuenta con una gran cantidad de paquetes gratis, 
así que no es común que se empiece un proceso desde 0 en cuanto a código. Además, 
la documentación de Node está muy bien elaborada y es posible encontrar cómo 
funcionan estos paquetes en el sitio web de Node.

En particular, aprendí un poco sobre Yargs, que es un paquete que facilita la
la obtención de parámetros al ejecutar el programa, de esta manera es posible
trabajar con datos que el usuario ingresa. En este caso, se utilizaron los argumentos
que se declaraban como comandos; dichos comandos pueden ser sus atributos (un alias,
si es obligatorio o no, una descripción, etc.).