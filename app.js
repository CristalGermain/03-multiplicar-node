const { crearArchivo, listar } = require('./multiplicar/multiplicar');
const argv = require('./config/yargs').argv;
const colors = require('colors');

//console.log('Límite: ', argv.limite); // Esta línea mostrará el valor del límite (por default 10)
//console.log('Base: ', argv.base); // Mostrará SÓLO el número que se le asigne a base al momento de escribir el comando que ejecuta este archivo (node app listar)

let comando = argv._[0]; //Trae el primer parámetro de argv, el cual va a determinar qué acción debe hacer el programa y es escrito por el usuario


switch (comando) {
    case 'listar':
        listar(argv.base, argv.limite);
        break;
    case 'crear':
        crearArchivo(argv.base, argv.limite)
            .then(archivo => console.log(`Archivo creado: ${archivo}`))
            .catch(err => console.log(err));
        break;
    default:
        console.log('Comando no reconocido');

}