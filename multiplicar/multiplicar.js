//Libreria importada para trabajar con archivos
const fs = require('fs');
const colors = require('colors');


let listar = (base, limite) => {

    console.log('============'.green);
    console.log(`Tabla del ${base}`.green);
    console.log('============'.green);
    for (let x = 1; x <= limite; x++) {
        console.log(`${base} x ${x} = ${base * x}`);
    }
};


let crearArchivo = (base, limite) => {
    return new Promise((resolve, reject) => {
        let data = '';

        //Number devuelve true si el parámetro que se le manda es un número
        if (!Number(base)) {
            reject(`ERROR El valor introducido como base no es un número`);
        } else {

            for (let x = 1; x <= limite; x++) {

                data += `${base} x ${x} = ${base * x} \n`;
            }

            fs.writeFile(`./tablas/tabla-${base}.txt`, data, (err) => {
                if (err) reject(err)
                else resolve(`tabla-${base}.txt`.green);
            });
        }
    });
};

module.exports = {
    crearArchivo,
    listar
}